import * as vscode from 'vscode';
import * as gitlab from './gitlab_functions';

const visibilityOptions = [
  {
    label: 'Public',
    type: 'public',
  },
  {
    label: 'Internal',
    type: 'internal',
  },
  {
    label: 'Private',
    type: 'private',
  },
];

// this method is called on create snippet command, 
// asks user input for snippet title, description
// asks if snippet will be public, internal or private
// creates snippet in gitlab
async function createSnippet(editor:any, snippetTitle:string | undefined, snippetDesc:string | undefined, snippetFileName:string | undefined, visibility:string, context:string) {
  let content = '';
  
  if (context === 'selection' && editor.selection) {
    const { start, end } = editor.selection;
    const endLine = end.line + 1;
    const startPos = new vscode.Position(start.line, 0);
    const endPos = new vscode.Position(endLine, 0);
    const range = new vscode.Range(startPos, endPos);
    content = editor.document.getText(range);
  }
  // else {
  //   content = editor.document.getText();
  // }
  await gitlab.createSnippet({
    title: snippetTitle,
    description: snippetDesc,
    file_name: snippetFileName,
    content: content,
    visibility,
  });
}

// show input box to add snippet
// asks for snippet title, description, visibility level
export async function showAddSnippet() {
  const editor:any = vscode.window.activeTextEditor;

  // get filetype of currently opened file, for use in snippet filename
  let fileType = editor.document.fileName.split('.').reverse()[0];
  if(fileType === '' || fileType === undefined) {
    fileType = 'md';
  }
  if (editor) {
    
    let snippetTitle = await vscode.window.showInputBox({
      ignoreFocusOut: true,
      value: 'Snippet Title',
      placeHolder: 'E.g. Sample snippet',
      prompt: 'Enter snippet title',
    });
  
    let snippetDesc = await vscode.window.showInputBox({
      ignoreFocusOut: true,
      value: 'Description',
      placeHolder: 'E.g. Sample snippet description',
      prompt: 'Enter snippet description',
    });

    // get filename from user input
    // let snippetFileName = await vscode.window.showInputBox({
    //   ignoreFocusOut: true,
    //   value: 'snippet.txt',
    //   placeHolder: 'E.g. snippet.txt',
    //   prompt: 'File name of your snippet',
    // });
    
    // prepare filename
    let snippetFileName = 'snippet.'+fileType;

    if(snippetTitle === '') {
      snippetTitle = 'Snippet Title';
    }
    if(snippetDesc === '') {
      snippetTitle = 'Snippet Description';
    }
  
    // get visibility from user input
    const visibility = await vscode.window.showQuickPick(visibilityOptions);

    if (visibility) {
      const context = 'selection';

      if (context) {
        createSnippet(editor, snippetTitle, snippetDesc, snippetFileName, visibility.type, context);
      }
    }
  } else {
    vscode.window.showInformationMessage('GitLab Snippets: No open file.');
  }
}
