// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as snippet_input from './snippet_input';
import * as accesstoken from './accesstoken';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "gitlab-snippets" is now active!');
	
	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable1 = vscode.commands.registerCommand('extension.gitlab-snippets-addsnippet', () => {
		// The code you place here will be executed every time your command is executed
		const instanceUrl = vscode.workspace.getConfiguration().get('gitlab-snippets.instanceUrl');
		const glToken = vscode.workspace.getConfiguration().get('gitlab-snippets.accessToken');
		
		// if no token, ask for token and continue adding snippet
		if (!glToken || !instanceUrl) {
			accesstoken.askForToken(true);
		} else {
			snippet_input.showAddSnippet();
		}
	});

	// handle commands
	let disposable2 = vscode.commands.registerCommand('extension.gitlab-snippets-settoken', (continueaddsnippet: boolean = false) => {
		// The code you place here will be executed every time your command is executed
		accesstoken.showInput(continueaddsnippet);
	});

	let disposable3 = vscode.commands.registerCommand('extension.gitlab-snippets-removetoken', () => {
		// The code you place here will be executed every time your command is executed
		accesstoken.removeToken();
	});

	context.subscriptions.push(disposable1);
	context.subscriptions.push(disposable2);
	context.subscriptions.push(disposable3);
}

// this method is called when your extension is deactivated
export function deactivate() {}
