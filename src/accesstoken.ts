import * as vscode from 'vscode';
import * as snippet_input from './snippet_input';

// show input box to add token and url
export async function showInput(continueaddsnippet: boolean = false) {
  const instance = await vscode.window.showInputBox({
    ignoreFocusOut: true,
    value: 'https://gitlab.com',
    placeHolder: 'E.g. https://gitlab.com',
    prompt: 'URL to Gitlab instance',
  });

  const token = await vscode.window.showInputBox({
    ignoreFocusOut: true,
    password: true,
    placeHolder: 'Paste your GitLab Personal Access Token...',
  });

  if (instance && token) {
    setToken(instance, token);

    // show add snipept after setToken, if settoken was initialized on clicking "Add snippet" context menu.
    if(continueaddsnippet) {
      snippet_input.showAddSnippet();
    }
  }
}

// remove token function, which removed token and url from settings.
export async function removeToken() {
  const config = vscode.workspace.getConfiguration('gitlab-snippets');
  // set settings to undefined / unset actually
  config.update('instanceUrl', undefined, 1);
  config.update('accessToken', undefined, 1);
}

const gitlabInstanceUrl = () => vscode.workspace.getConfiguration('gitlab-snippets').instanceUrl;
const gitlabAccessToken = () => vscode.workspace.getConfiguration('gitlab-snippets').accessToken;

export const getToken = gitlabAccessToken;

export const getInstanceUrls = gitlabInstanceUrl;

// set token function, which adds token and url from settings.
export const setToken = (instanceUrl:string, token:string) => {
  const config = vscode.workspace.getConfiguration('gitlab-snippets');

  // retrieve values
  config.update('instanceUrl', instanceUrl, 1);
  config.update('accessToken', token, 1);
};


// ask for token, when user clicks add snippet option and no token is set
export const askForToken = (continueaddsnippet: boolean = false) => {
  if (!getToken()) {
    const message =
      'GitLab Snippets: Please set GitLab Personal Access Token & Instance URL to setup this extension.';
    const setButton = { title: 'Set Token Now', action: 'set' };
    const readMore = { title: 'Read More', action: 'more' };

    vscode.window.showInformationMessage(message, readMore, setButton).then(item => {
      if (item) {
        const { action } = item;

        if (action === 'set') {
          vscode.commands.executeCommand('extension.gitlab-snippets-settoken', continueaddsnippet);
        } else {
          vscode.commands.executeCommand('vscode.open', vscode.Uri.parse('https://google.com'));
        }
      }
    });
  }
};