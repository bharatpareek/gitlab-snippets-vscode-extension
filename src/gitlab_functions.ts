import * as vscode from 'vscode';
import * as request from "request";

// call gitlab API and return response in callback to caller function
async function fetch(path:string, method = 'GET', data:any, action_message:string = '', callback: (err:boolean, body:string) => void) {
  const instanceUrl = vscode.workspace.getConfiguration().get('gitlab-snippets.instanceUrl');
  const glToken = vscode.workspace.getConfiguration().get('gitlab-snippets.accessToken');

  if (!glToken || !instanceUrl) {
    return vscode.window.showInformationMessage(
      'GitLab Snippets: Access token or instance URL is missing!',
    );
  }

  // build api url
  const apiRoot = instanceUrl+path;
  
  // build api options
  const options = {
    url: apiRoot,
    method: method,
    headers: {
      'PRIVATE-TOKEN': glToken,
    },
    form: undefined
  };

  // if has post data
  if(data) {
    options.form = data;
  }
  
  // show message that creation started
  if(action_message !== '') {
    var statusbarmessage = vscode.window.setStatusBarMessage(action_message);
  }
  
  // make api request to add snippet
  request(options, function(error:any, response:any, body:any) {
    
    // hide status bar message
    if(statusbarmessage) {
      statusbarmessage.dispose();
    }

    if(error) { // error in adding snippet
        //vscode.window.showErrorMessage('GitLab Snippets: Some error occurred.');
        return callback(error, '');
    } else if (!error && (response.statusCode === 200 || response.statusCode === 201)) { // request successful
        return callback(false, body);
    } else {  // error in adding snippet
        vscode.window.showErrorMessage('GitLab Snippets: Some error occurred.');
    }
  });
}

// get snippet list from gitlab
// export async function getSnippets(type: string = 'public') {
//   let path: string = '';
//   let action_message = 'GitLab Snippets: Fetching snippet list.';

//   if(type === 'user') {
//     path = '/api/v4/snippets';
//   } else {
//     path = '/api/v4/snippets/public';
//   }

//   try {
//     await fetch(path, 'get', [], action_message, function(err:boolean, body:string) {
//       if(!err) {
//         return;
//       } else {
//         vscode.window.showInformationMessage('GitLab Snippets: Failed to create your snippet.');
//       }
//     });
//   } catch (e) {
//     vscode.window.showInformationMessage('GitLab Snippets: Failed to get snippets.');
//   }
// }

// calls function to make create snippet API call
export async function createSnippet(data:any) {

  let action_message = 'GitLab Snippets: Creating snippet on GitLab.';
  try {
    await fetch('/api/v4/snippets', 'post', data, action_message, function(err:boolean, body:string) {
      
      if(!err) {
        // show success message box
        const message = 'GitLab Snippets: Snippet created successfully.';
        const viewSnippetButton = { title: 'View Snippet', action: 'view-snippet' };
        let body_parsed = JSON.parse(body);
  
        vscode.window.showInformationMessage(message, viewSnippetButton).then(item => {
          if (item) {
            const { action } = item;
  
            if (action === 'view-snippet') {
              vscode.commands.executeCommand('vscode.open', vscode.Uri.parse(body_parsed.web_url));
            }
          }
        });
      } else {
        vscode.window.showErrorMessage('GitLab Snippets: Failed to create your snippet.');
      }
    });
  } catch (e) {
    vscode.window.showErrorMessage('GitLab Snippets: Failed to create your snippet.');
  }
}