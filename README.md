# GitLab Snippets README

The sole purpose of this extension is to ease code reusability, by providing an easy way to add specific lines of code to GitLab as a snippet.

## Features

- Easily add selected lines of code to GitLab as snippet.
  - Provides option to choose snippet title, description and visibility level.
  - Shows message in status bar when create snippet action is performed.
  - Shows a link to view created snippet on success.

**Want to know about upcoming features?** [Check it out](#whats-next)

## Key Bindings
- Keyboard shortcut to execute create snippet action.
  - On Windows & Linux 'ctrl+f1'.
  - On Mac 'cmd=f1'.

## Screenshots

![img](https://i.imgur.com/l65tCrk.png "Step 1")

![img](https://i.imgur.com/tSHDuQl.png "Step 2")

![img](https://i.imgur.com/uRevGRJ.png "Step 3")


## Setup

You will need to create a GitLab Personal Access Token to use this extension. The token works as means of authentication for API request.

##### Step 1: Create your Personal Access Token
- If you are using
  - GitLab.com [click to open Personal Access Tokens page](https://gitlab.com/profile/personal_access_tokens).
  - Self-hosted GitLab instance go to "GitLab URL/profile/personal_access_tokens".
- On "Add a personal access token" form
  - Give a name to your token.
  - Select and expiry date.
  - Select "api" and "read_user" permissions.
  - Press "Create personal access token" button.
- Copy the token. _Remember you won't be able to see the value of this token ever again for security reasons._

##### Step 2: Add token to GitLab Snippets Extension
- Open up Command Palette by pressing `Cmd+Shift+P`.
- Search for "GitLab Snippets: Set Personal Access Token" and press Enter.
- Enter the URL to the Gitlab instance and press Enter.
- Extension will ask for your Token, paste and press Enter.

Now select some lines of code, right click and select "GitLab Snippets: Add GitLab Snippet". The snippet will be created on your GitLab instance.

## Notes

- Option to add snippet will only be shown if there is some code/text is selected in editor.

## What's next?
- A sidebar panel to display all public and user snippets from GitLab as well as snippets from VSCode snippet feature.
- Easily search and select a snippet from sidebar and insert into editor.
- Edit the snippets directly from VSCode, eliminating the need to go to GitLab for that.

### 0.0.1

Initial beta release of GitLab Snippets VSCode extension.

**Enjoy!**
